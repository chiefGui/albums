import * as React from "react"
import * as ReactDOM from "react-dom"
import axios from "axios"
import styled from "styled-components"

const App: React.FC = (): JSX.Element => {
  const [mostRecentPhotos, setMostRecentPhotos] = React.useState<IPhoto[]>([])

  React.useEffect((): void => {
    ;(async (): Promise<void> => {
      const { data: albums } = await fetchAlbums()
      const mostRecentPhotos = getMostRecentPhotos(albums)

      setMostRecentPhotos(mostRecentPhotos)
    })()
  }, [])

  if (mostRecentPhotos.length === 0) {
    return <div>loading...</div>
  }

  return (
    <Container>
      {mostRecentPhotos.map(
        (photo): JSX.Element => (
          <Photo key={photo.id} photo={photo} />
        ),
      )}
    </Container>
  )
}

const Photo: React.FC<IPhotoComponent> = ({ photo }): JSX.Element => {
  return (
    <PhotoContainer
      style={{ border: `5px solid ${BORDER_COLORS[photo.albumId]}` }}
    >
      <Title>{photo.title}</Title>
      <Img src={photo.thumbnailUrl} alt={photo.title} />
    </PhotoContainer>
  )
}

async function fetchAlbums(): Promise<IFetchAlbumsPayload> {
  return axios.get("https://jsonplaceholder.typicode.com/photos")
}

function getMostRecentPhotos(photos: IPhoto[]): IPhoto[] {
  return photos
    .reverse()
    .filter((photo): boolean => MOST_RECENT_ALBUMS_IDS.includes(photo.albumId))
}

const MOST_RECENT_ALBUMS_IDS = [100, 99, 98]
const BORDER_COLORS = {
  [MOST_RECENT_ALBUMS_IDS[0]]: "green",
  [MOST_RECENT_ALBUMS_IDS[1]]: "blue",
  [MOST_RECENT_ALBUMS_IDS[2]]: "purple",
}

interface IFetchAlbumsPayload {
  data: IPhoto[]
}

interface IPhoto {
  albumId: number
  id: number
  title: string
  url: string
  thumbnailUrl: string
}

interface IPhotoComponent {
  photo: IPhoto
}

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const PhotoContainer = styled.div`
  position: relative;
  margin: 20px;
  width: 150px;
  height: 150px;
`

const Title = styled.h4`
  position: absolute;
  top: 0;
  left: 0;
  margin: 20px;
  color: white;
`

const Img = styled.img`
  display: block;
  width: 100%;
  height: 100%;
`

ReactDOM.render(<App />, document.getElementById("root"))
